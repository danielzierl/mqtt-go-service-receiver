package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/danielzierl/mqtt-go-service-receiver/internal"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/logging"
)

func main() {
	//Setting up logging
	logging.SetupLoggingAuto("reciever")
	defer logging.CloseLogFile()

	err := godotenv.Load()
	if err != nil {
		log.Fatal(".env not found")
	}
	// db := internal.ConnectDb()
	internal.ReceiveMessages()
}
