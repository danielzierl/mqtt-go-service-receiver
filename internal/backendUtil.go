package internal

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

var moving_temperature_average *float64 = nil
var moving_humidity_average *float64 = nil

func preprocess(val float64, moving_average *float64) float64 {
	if moving_average == nil {
		moving_average = &val
	}
	*moving_average = (*moving_average + val) / 2
	return *moving_average
}

const BACKEND_POST_READING_ENDPOINT = "/readings"

func SendSensorReadingToBackEnd(processed_reading *models.SensorReading) {

	processed_reading.Temperature = preprocess(processed_reading.Temperature, moving_temperature_average)
	processed_reading.Humidity = preprocess(processed_reading.Humidity, moving_humidity_average)

	body, err := json.Marshal(processed_reading)
	if err != nil {
		log.Printf("Failed to marshal sensor reading post: %s", err)
	}
	responseBody := bytes.NewBuffer(body)
	resp, err := http.Post(os.Getenv("BACKEND_API_URL")+BACKEND_POST_READING_ENDPOINT, "application/json", responseBody)
	if err != nil {
		log.Printf("Failed to post sensor reading: %s", err)
		return
	}
	defer resp.Body.Close()
}
