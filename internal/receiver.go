package internal

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	recievermodels "gitlab.com/danielzierl/mqtt-go-service-receiver/reciever_models"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

var TOPIC string
var SUPPORT_TOPIC string

func float64frombytes(bytes []byte) float64 {
	bits := binary.BigEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}
var consecutive_counter int = 0
var messagePubHandler = func(client mqtt.Client, msg mqtt.Message) {
	var reading recievermodels.Reading
	err := json.Unmarshal(msg.Payload(), &reading)

	if err != nil {
		log.Printf("Failed to parse temperature: %s", err)
	}

	processed_reading := &models.SensorReading{
		TeamName:     reading.TeamName,
		Timestamp:    reading.Timestamp,
    Temperature:  reading.Temperature,
    Humidity:     reading.Humidity,
    Illumination: reading.Illumination,
    Consecutive:  consecutive_counter,
    ConsecutivePrune:   consecutive_counter,
	}
  consecutive_counter++

	SendSensorReadingToBackEnd(processed_reading)
	fmt.Println("Received message: ", string(msg.Payload()))

}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
  log.Printf("Connected to broker %s", os.Getenv("BROKER_HOST"))
  subscribe(client)
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	log.Printf("Connection to broker lost: %v", err)
}

func connectBroker() mqtt.Client {
	TOPIC = os.Getenv("TOPIC")
	SUPPORT_TOPIC = os.Getenv("SUPPORT_TOPIC")
	var broker = os.Getenv("BROKER_HOST")
	var port = os.Getenv("BROKER_PORT")

	opts := mqtt.NewClientOptions()
	opts.SetUsername(os.Getenv("BROKER_USER"))
	opts.SetPassword(os.Getenv("BROKER_PASSWORD"))
	opts.AddBroker(fmt.Sprintf("tcp://%s:%s", broker, port))
	opts.SetClientID(os.Getenv("BROKER_CLIENT_ID"))
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
		messagePubHandler(client, msg) // Pass the 'db' argument to the message handler
	})
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	return mqtt.NewClient(opts)
}
func ReceiveMessages() {
	var client mqtt.Client
	for {
		if client == nil || !client.IsConnected() {
			log.Println("Connecting to broker", os.Getenv("BROKER_HOST"))
			client = connectBroker()
			if token := client.Connect(); token.Wait() && token.Error() != nil {
				log.Println("Error while connecting to broker: ", token.Error())
				continue
			}
		}
		time.Sleep(5 * time.Second)
	}
}

func subscribe(client mqtt.Client) {
	token := client.Subscribe(SUPPORT_TOPIC, 1, nil)
	token.Wait()
	log.Printf("Subscribed to topic: %s\n", SUPPORT_TOPIC)
}
