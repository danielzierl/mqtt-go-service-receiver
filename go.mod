module gitlab.com/danielzierl/mqtt-go-service-receiver

go 1.21.1

require (
	github.com/eclipse/paho.mqtt.golang v1.4.3
	github.com/joho/godotenv v1.5.1
	gitlab.com/danielzierl/sensor-reading-shared-libs v1.2.0
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sync v0.3.0 // indirect
	gorm.io/driver/sqlite v1.5.4 // indirect
	gorm.io/gorm v1.25.5 // indirect
)
