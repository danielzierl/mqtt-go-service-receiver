package recievermodels

type Reading struct {
	TeamName     string  `json:"team_name"`
	Timestamp    string  `json:"timestamp"`
	Temperature  float64 `json:"temperature,omitempty"`
	Humidity     float64 `json:"humidity,omitempty"`
	Illumination float64 `json:"illumination,omitempty"`
}
